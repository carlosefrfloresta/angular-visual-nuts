import { CountryLanguageService, Country, CountryEnum, LanguageEnum } from "../../../src/app/functions/function-two";

const { DUTCH, ENGLISH, FRENCH, FRISIAN, GERMAN, SPANISH } = LanguageEnum

const MOCK_LIST: Country[] = [
    {
        country: CountryEnum.UNITED_STATES,
        languages: [ENGLISH]
    },
    {
        country: CountryEnum.BELGIUM,
        languages: [DUTCH, FRENCH, GERMAN]
    },
    {
        country: CountryEnum.NETHERLANDS,
        languages: [DUTCH, FRISIAN]
    },
    {
        country: CountryEnum.GERMANY,
        languages: [GERMAN]
    },
    {
        country: CountryEnum.SPAIN,
        languages: [SPANISH]
    }
]

const MOCK_LANGUAGE_NUMBER = 6;

describe('CountryLanguageService', () => {
    let countryLanguageService: CountryLanguageService;

    beforeEach(() => countryLanguageService = new CountryLanguageService());

    describe('country', () => {
        it('should add list do attribute _countries', () => {
            countryLanguageService.countries = MOCK_LIST;
            expect(countryLanguageService.countries).toEqual(MOCK_LIST);
        });
    });

    beforeEach(() => countryLanguageService.countries = MOCK_LIST);

    describe('getCountryNumber', () => {
        it('should call without errors', () => {
            const spy = jest.spyOn(countryLanguageService, 'getCountryNumber');
            countryLanguageService.getCountryNumber();
            expect(spy).toBeCalled();
        });

        it(`should be equal to ${MOCK_LIST.length}`, () => {
            const number = countryLanguageService.getCountryNumber();
            expect(number).toEqual(MOCK_LIST.length);
        });
    });

    describe('countLanguages', () => {
        it('should call without errors', () => {
            const spy = jest.spyOn(countryLanguageService, 'countLanguages');
            countryLanguageService.countLanguages();
            expect(spy).toBeCalled();
        });

        it(`should be equal to ${MOCK_LANGUAGE_NUMBER}`, () => {
            const count = countryLanguageService.countLanguages();
            expect(count).toEqual(MOCK_LANGUAGE_NUMBER);
        });
    });

    describe('getMostDiverseCountry', () => {
        it('should call without errors', () => {
            const spy = jest.spyOn(countryLanguageService, 'getMostDiverseCountry');
            countryLanguageService.getMostDiverseCountry();
            expect(spy).toBeCalled();
        });

        it(`should be equal to ${CountryEnum.BELGIUM}`, () => {
            const mostDiverseCountry = countryLanguageService.getMostDiverseCountry();
            expect(mostDiverseCountry).toEqual(CountryEnum.BELGIUM);
        });

        it(`receives ${FRISIAN} and should be equal to ${CountryEnum.NETHERLANDS}`, () => {
            const mostDiverseCountry = countryLanguageService.getMostDiverseCountry(FRISIAN);
            expect(mostDiverseCountry).toEqual(CountryEnum.NETHERLANDS);
        });
    });

    describe('getMostSpoken', () => {
        it('should call without errors', () => {
            const spy = jest.spyOn(countryLanguageService, 'getMostSpoken');
            countryLanguageService.getMostSpoken();
            expect(spy).toBeCalled();
        });

        it(`should be equal to ${DUTCH} ans ${GERMAN}`, () => {
            const mostSpoken = countryLanguageService.getMostSpoken();
            expect(mostSpoken).toEqual([DUTCH, GERMAN]);
        });
    });
});
