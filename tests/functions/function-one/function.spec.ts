import * as functionOne from "../../../src/app/functions/function-one";


const MOCK_VALUE = [
    1, 2, 'Visual', 4, 'Nuts',
    'Visual', 7, 8, 'Visual', 'Nuts',
    11, 'Visual', 13, 14, 'Visual Nuts',
    16, 17, 'Visual', 19, 'Nuts',
    'Visual', 22, 23, 'Visual', 'Nuts',
    26, 'Visual', 28, 29, 'Visual Nuts',
    31, 32, 'Visual', 34, 'Nuts',
    'Visual', 37, 38, 'Visual', 'Nuts',
    41, 'Visual', 43, 44, 'Visual Nuts',
    46, 47, 'Visual', 49, 'Nuts',
    'Visual', 52, 53, 'Visual', 'Nuts',
    56, 'Visual', 58, 59, 'Visual Nuts',
    61, 62, 'Visual', 64, 'Nuts',
    'Visual', 67, 68, 'Visual', 'Nuts',
    71, 'Visual', 73, 74, 'Visual Nuts',
    76, 77, 'Visual', 79, 'Nuts',
    'Visual', 82, 83, 'Visual', 'Nuts',
    86, 'Visual', 88, 89, 'Visual Nuts',
    91, 92, 'Visual', 94, 'Nuts',
    'Visual', 97, 98, 'Visual', 'Nuts'
]

describe('printVisualNuts', () => {
    describe('generateVisualNuts', () => {
        it('should call without errors', () => {
            const spy = jest.spyOn(functionOne, 'generateVisualNuts');
            functionOne.generateVisualNuts();
            expect(spy).toBeCalled();
        });

        it('should be equal to MOCK_VALUE', () => {
            const value = functionOne.generateVisualNuts();
            expect(value).toEqual(MOCK_VALUE);
        });
    });

    describe('printVisualNuts', () => {
        beforeEach(() => { console.log = jest.fn(); })

        it('should call without errors', () => {
            const spy = jest.spyOn(functionOne, 'printVisualNuts');
            functionOne.printVisualNuts();
            expect(spy).toBeCalled();
        });

        it('should have been called with MOCK_VALUE', () => {
            const consoleSpy = jest.spyOn(console, 'log');
            functionOne.printVisualNuts();
            expect(consoleSpy).toHaveBeenCalledWith(MOCK_VALUE);
        });
    });
});