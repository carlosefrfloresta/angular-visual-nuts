import { Component } from '@angular/core';
import { printVisualNuts } from './functions/function-one';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-visual-nuts';
}
