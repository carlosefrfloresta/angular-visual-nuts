import { Injectable } from "@angular/core";

export enum LanguageEnum {
    DUTCH = 'nl',
    FRENCH = 'fr',
    GERMAN = 'de',
    FRISIAN = 'fy',
    ENGLISH = 'en',
    SPANISH = 'es'
}

export enum CountryEnum {
    UNITED_STATES = 'US',
    BELGIUM = 'BE',
    NETHERLANDS = 'NL',
    GERMANY = 'DE',
    SPAIN = 'ES',
}

export interface Country {
    country: CountryEnum;
    languages: LanguageEnum[]
}

/**
 * @author Carlos Ramos
 * @since 07/2021
 * 
 * @description 
 * Class used to provide a list of official languages by country to the application.
 * 
 * @usecases
 * 
 * ```ts
 * constructor(private _countryLanguageService: CountryLanguageService) { }
 * ```
 */
@Injectable()
export class CountryLanguageService {


    public set countries(countries: Country[]) {
        this._countries = countries;
        this._languages = null;
    }

    public get countries(): Country[] {
        return this._countries;
    }

    /**
     * List of countries with their respectives spoken languages
     */
    private _countries: Country[];

    /**
     * Return an untreated list of spoken languages. 
     * Generating the list once if the attribute _languages is null.
     * @returns LanguageEnum[]
     */
    private _getLanguages(): LanguageEnum[] {
        if (!this._languages) {
            this._languages = this._countries.reduce((acc, act) => [...acc, ...act.languages], []);
        }
        return this._languages;
    }

    private _languages: LanguageEnum[];


    constructor() { }


    /**
     * Return the number of countries in the service
     * @returns number
     */
    public getCountryNumber(): number {
        return this._countries.length;
    }

    /**
     * Method that returns the country with the most number of languages.
     * @param language LanguageEnum
     * @returns string
     */
    public getMostDiverseCountry(language?: LanguageEnum): string {
        if (language) {
            const response = [...this._countries].sort((a, b) => {
                const aHasLanguage = a.languages.includes(language);
                const bHasLanguage = b.languages.includes(language);
                if (aHasLanguage && bHasLanguage) {
                    return a.languages.length > b.languages.length ? 1 : -1;
                } else {
                    return aHasLanguage ? 1 : bHasLanguage ? -1 : 1;
                }
            }).pop()

            return response.languages.includes(language) ? response.country : '';
        } else {
            const response = [...this._countries].sort((a, b) =>
                a.languages.length > b.languages.length ? 1 : -1
            ).pop()

            return response.country;
        }
    }

    /**
     * Method that returns the number of distinct languages.
     * @returns number
     */
    public countLanguages(): number {
        const langSet = new Set(this._getLanguages());
        return langSet.size
    }

    /**
     * Method that returns a list with the most spoken languages.
     * @returns LanguageEnum[]
     */
    public getMostSpoken(): LanguageEnum[] {
        let mostSpoken: LanguageEnum[][] = [];
        this._getLanguages().reduce((acc, act) => {
            acc[act] = acc[act] ? acc[act] + 1 : 1;
            if (!mostSpoken[acc[act]]) {
                mostSpoken[acc[act]] = []
            }
            mostSpoken[acc[act]].push(act);
            return acc;
        }, {})

        return mostSpoken.pop();
    }
}