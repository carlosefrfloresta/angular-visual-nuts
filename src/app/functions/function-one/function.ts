
/**
 * Function that generate a list of number, replacing values by the string 'Nuts' and 'Visual' based on the visualDivisor and nutsDivisor parameters.
 * @param start number
 * @param end number
 * @param visualDivisor number
 * @param nutsDivisor number
 * @returns (string | number)[]
 */
export function generateVisualNuts(
    start: number = 1,
    end: number = 100,
    visualDivisor: number = 3,
    nutsDivisor: number = 5,
): (string | number)[] {
    const limit: number = end + 1;
    const values: (string | number)[] = [];

    for (let i = start; i < limit; i++) {
        let value = i % nutsDivisor === 0 ? ' Nuts' : '';
        value = i % visualDivisor === 0 ? `Visual${value}` : value.trim();
        if (value) {
            values.push(value);
        } else {
            values.push(i);
        }
    }

    return values;
}


/**
 * Function that prints the return from the generateVisualNuts function.
 * @param start number
 * @param end number
 * @param visualDivisor number
 * @param nutsDivisor number
 */
export function printVisualNuts(
    start?: number,
    end?: number,
    visualDivisor?: number,
    nutsDivisor?: number,
): void {
    console.log(generateVisualNuts(
        start,
        end,
        visualDivisor,
        nutsDivisor,
    ))
}