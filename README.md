# AngularVisualNuts

An Angular 11 project that uses Jest to test two features, called here as `Functions`.

## Running the tests

* Clone or download the repository;
* Install the dependencies using `npm install` or `yarn install`;
* Run `npm test` or `yarn test` to start the test. 

## Function One
This feature is responsable by generating and printing a vector of values, and it's separeted in two functions.

The first one, `generateVisualNuts`, generate a list of number, replacing values by the string 'Nuts' and 'Visual' based on the visualDivisor and nutsDivisor parameters.

The second one, `printVisualNuts`, is a function that call internally the first one and prints it's result.

[generateVisualNut](https://gitlab.com/carlosefrfloresta/angular-visual-nuts/-/blob/master/src/app/functions/function-one/function.ts)
```ts
export function generateVisualNuts(
    start: number = 1,
    end: number = 100,
    visualDivisor: number = 3,
    nutsDivisor: number = 5,
): (string | number)[] {
    const limit: number = end + 1;
    const values: (string | number)[] = [];

    for (let i = start; i < limit; i++) {
        let value = i % nutsDivisor === 0 ? ' Nuts' : '';
        value = i % visualDivisor === 0 ? `Visual${value}` : value.trim();
        if (value) {
            values.push(value);
        } else {
            values.push(i);
        }
    }

    return values;
}
```

[printVisualNuts](https://gitlab.com/carlosefrfloresta/angular-visual-nuts/-/blob/master/src/app/functions/function-one/function.ts)
```ts
export function printVisualNuts(
    start?: number,
    end?: number,
    visualDivisor?: number,
    nutsDivisor?: number,
): void {
    console.log(generateVisualNuts(
        start,
        end,
        visualDivisor,
        nutsDivisor,
    ))
}
```

[Test file for this function.](https://gitlab.com/carlosefrfloresta/angular-visual-nuts/-/blob/master/tests/functions/function-one/function.spec.ts)

## Function two
This feature was build as an Angular service, that receives a context and works with it to provide various values based on a list of objects that represent countries and their spoken languages. It was built this way to give single responsibility to each method.

[CountryLanguageService](https://gitlab.com/carlosefrfloresta/angular-visual-nuts/-/blob/master/src/app/functions/function-two/country.service.ts)
```ts
@Injectable()
export class CountryLanguageService {


    public set countries(countries: Country[]) {
        this._countries = countries;
        this._languages = null;
    }

    public get countries(): Country[] {
        return this._countries;
    }

    /**
     * List of countries with their respectives spoken languages
     */
    private _countries: Country[];

    /**
     * Return an untreated list of spoken languages. 
     * Generating the list once if the attribute _languages is null.
     * @returns LanguageEnum[]
     */
    private _getLanguages(): LanguageEnum[] {
        if (!this._languages) {
            this._languages = this._countries.reduce((acc, act) => [...acc, ...act.languages], []);
        }
        return this._languages;
    }

    private _languages: LanguageEnum[];


    constructor() { }


    /**
     * Return the number of countries in the service
     * @returns number
     */
    public getCountryNumber(): number {
        return this._countries.length;
    }

    /**
     * Method that returns the country with the most number of languages.
     * @param language LanguageEnum
     * @returns string
     */
    public getMostDiverseCountry(language?: LanguageEnum): string {
        if (language) {
            const response = [...this._countries].sort((a, b) => {
                const aHasLanguage = a.languages.includes(language);
                const bHasLanguage = b.languages.includes(language);
                if (aHasLanguage && bHasLanguage) {
                    return a.languages.length > b.languages.length ? 1 : -1;
                } else {
                    return aHasLanguage ? 1 : bHasLanguage ? -1 : 1;
                }
            }).pop()

            return response.languages.includes(language) ? response.country : '';
        } else {
            const response = [...this._countries].sort((a, b) =>
                a.languages.length > b.languages.length ? 1 : -1
            ).pop()

            return response.country;
        }
    }

    /**
     * Method that returns the number of distinct languages.
     * @returns number
     */
    public countLanguages(): number {
        const langSet = new Set(this._getLanguages());
        return langSet.size
    }

    /**
     * Method that returns a list with the most spoken languages.
     * @returns LanguageEnum[]
     */
    public getMostSpoken(): LanguageEnum[] {
        let mostSpoken: LanguageEnum[][] = [];
        this._getLanguages().reduce((acc, act) => {
            acc[act] = acc[act] ? acc[act] + 1 : 1;
            if (!mostSpoken[acc[act]]) {
                mostSpoken[acc[act]] = []
            }
            mostSpoken[acc[act]].push(act);
            return acc;
        }, {})

        return mostSpoken.pop();
    }
}
```

[Test file for this function.](https://gitlab.com/carlosefrfloresta/angular-visual-nuts/-/blob/master/tests/functions/function-two/country.service.spec.ts)

## More links

If anyone wants to see more of my work here is the link for one of my [projects](https://github.com/CarlosEduardoFerreiraRamos/angular-account-management), one with tests, CI pipeline and it is hosted on AWS Cloudfront.